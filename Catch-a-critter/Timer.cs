﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_critter
{
    class Timer
    {
        //-------------------------------------
        // data
        //-------------------------------------
       float timeRemaining = 0f;
        Vector2 position = new Vector2(200, 10);
        SpriteFont font = null;
        bool running = false;

        //delegates 
        // the delgate definition states what types of functions are allowed 
        public delegate void TimeUp();// declares a type
        public TimeUp ourTimerCallback;
        //-------------------------------------
        // behaviour
        //-------------------------------------

        public void Loadcontent(ContentManager content)
        {
            // load the font from file so it can be used to draw text
            font = content.Load<SpriteFont>("fonts/mainFont");
            //-------------------------------------


        }
        public void Draw(SpriteBatch spriteBatch)
        {
            // draw the score to the screen using the font variable 
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time:" + timeInt.ToString(), position, Color.White);
        }
        //-------------------------------------
        public void Update(GameTime gameTime)
        {
            if(running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                // if our time has run out...
                if(timeRemaining<= 0)
                {
                    // stop
                    running = false;
                    timeRemaining = 0;

                    //the something to do 
                    if (ourTimerCallback != null)
                        ourTimerCallback();
                }
            }
                
                
            
        }
        //-------------------------------------
        public void StartTimer()
        {
            running = true;
        }
        //-------------------------------------
        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
        //-------------------------------------

        
    }
}
