﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_critter
{
    class Score
    {
        //-------------------------------------
        // data
        //-------------------------------------
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;

        //-------------------------------------
        // behaviour
        //-------------------------------------
        public void Loadcontent(ContentManager content)
        {
            // load the font from file so it can be used to draw text
            font = content.Load<SpriteFont>("fonts/mainFont");
        }

        public void AddScore(int toAdd)
        {
            // adds the provided number to our current score value
            value = value + toAdd;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // draw the score to the screen using the font variable 
            spriteBatch.DrawString(font, "score:" + value.ToString(), position, Color.White);
        }
        //-------------------------------------
        public void resetScore()
        {
            value = 0;
        }
        //-------------------------------------
    }
}
