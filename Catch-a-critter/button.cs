﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;


namespace Catch_a_critter
{
    class button
    {
        //-------------------------------------
        // data
        //-------------------------------------
        Texture2D buttonGraphic = null;
        Vector2 position;
        SoundEffect click = null;
        bool visible = true;

        //delegates 
        // the delgate definition states what types of functions are allowed 
        public delegate void OnClick();
        public OnClick ourButtonCallback;

        //-------------------------------------
        // behaviour
        //-------------------------------------

        public void Loadcontent(ContentManager content)
        {
            buttonGraphic = content.Load<Texture2D>("graphics/button");
            click = content.Load<SoundEffect>("audio/buttonclick");
        }

        //-------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(buttonGraphic, position, Color.White);
            }

        }
        //-------------------------------------
        public void Input()
        {
            // get current mouse status
            MouseState currentState = Mouse.GetState();
            // get thebounding box for critter 
            Rectangle Bounds = new Rectangle((int)position.X, (int)position.Y, buttonGraphic.Width, buttonGraphic.Height);
            // check if left mouse button is held down over critter 
            if (currentState.LeftButton == ButtonState.Pressed && Bounds.Contains(currentState.X, currentState.Y) )
            {
                //we clicked the critter 


                //play SFX
                click.Play();

                // Hide the button
                visible = false;

                //TODO Start the game
                if(ourButtonCallback != null)
                ourButtonCallback();


            }

        }
        //-------------------------------------
        public void Show()
        {
            visible = true;
        }
        //-------------------------------------
    }
}
