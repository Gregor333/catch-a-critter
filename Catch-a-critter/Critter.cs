﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace Catch_a_critter
{
    class Critter
    {
        //-------------------------------------
        // Type Definitions 
        //-------------------------------------
        public enum Species
            // a special type of interger wit only specific values allowed that have names
        {
            CROCODILE,  // = 0 
            HORSE,      // = 1
            ELEPHANT,   // = 2

            //--

            NUM        // = 3 
        }


        //-------------------------------------
        // data
        //-------------------------------------

        Texture2D image = null;
        Vector2 position;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect click = null;
        Species ourSpecies = Species.CROCODILE;
        

        //-------------------------------------
        // behaviour
        //-------------------------------------
        public Critter(Score newScore, Species newSpecies)
        {
            // constructor called when object is created
            // no returen type(special)
            // helps decide how object the will be set up
            // can have arguments (such as newScore)
            // This one lets us have access to game score 
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }

        //-------------------------------------
        public void Loadcontent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/crocodile");

            switch(ourSpecies)
            {
                case Species.CROCODILE:
                    image = content.Load<Texture2D>("graphics/crocodile");
                    critterValue = 10;
                    break;
                case Species.HORSE:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 1;
                    break;
                case Species.ELEPHANT:
                     image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 50;
                    break;
                default:
                    //this sould never happen 
                    break;
            }

            click = content.Load<SoundEffect>("audio/buttonclick");
        }

        //-------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
            

        }
        //-------------------------------------
        public void spawn(GameWindow window)
        {
            //set the critter to alive 
            alive = true;
            //determine bounds for random location of the critter
            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Height - image.Height;

            //generate random location of the critter within the bounds
            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        //-------------------------------------
        public void Despawn()
        {
            // set critter to not alive in order to not draw it and not clickable
            alive = false;
        }
        //-------------------------------------
        public void Input()
        {
            // get current mouse status
            MouseState currentState = Mouse.GetState();
            // get thebounding box for critter 
            Rectangle critterBounds = new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            // check if left mouse button is held down over critter 
            if (currentState.LeftButton == ButtonState.Pressed && critterBounds.Contains(currentState.X, currentState.Y )&& alive == true)
            {
                //we clicked the critter 


                //play SFX
                click.Play();

                //despawn the critter
                Despawn();

                // add to score(TODO)
                scoreObject.AddScore(critterValue);
            }
        }

    }
}
