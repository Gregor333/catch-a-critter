﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Catch_a_critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20;
        Critter[] critterPool = new Critter[MAX_CRITTERS];// an array of critter references 
        Score ourScore; // a blank address for the players score
        button ourButton; // a blank address for our button 
        const float SPAWN_DELAY = 3f; // time between critter spawns
        float timeUntilNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        bool playing = false;
        Timer gameTimer = null;
        const float GAME_LENGTH = 30f;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            //create score object and load its content (fault)
            ourScore = new Score();
            ourScore.Loadcontent(Content);
            
            // create score object and load its content 
            ourButton = new button();
            ourButton.Loadcontent(Content);
            
            // create timer object and load its content
            gameTimer = new Timer();
            gameTimer.Loadcontent(Content);
            gameTimer.SetTimer(GAME_LENGTH);
            gameTimer.ourTimerCallback += EndGame;
            
            // set the function that will be called when the button was clicked
            //(+= means it adds to any existing functions set there)
            ourButton.ourButtonCallback += Startgame;

            //initialise random number generator for critter species
            Random rand = new Random();

            //creates critter objects and load their content
            for(int i = 0; i < MAX_CRITTERS; ++ i)
            {
                Critter.Species newSpecies = (Critter.Species)rand.Next(0, (int)Critter.Species.NUM);

                // creating a new critter and giving it the score
                Critter newCritter = new Critter(ourScore, newSpecies );
                
                // loading cibtent for critter(image/sfx)
                newCritter.Loadcontent(Content);

                //add newly created critter to our pool
                critterPool[i] = newCritter;
            }
           
            //ourCritter.image = Content.Load<Texture2D>("graphics/crocodile");
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
         
          

            if(playing== true)
            {
                gameTimer.Update(gameTime);
               
                foreach (Critter eachCritter in critterPool)
                {
                    //loops contents 
                    eachCritter.Input();

                }
                timeUntilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilNextSpawn <= 0f)
                {
                    //reset spawn timer
                    timeUntilNextSpawn = SPAWN_DELAY;
                    //spawn a new critter


                    //spawn next criiter in list 
                    critterPool[currentCritterIndex].spawn(Window);
                    ++currentCritterIndex;
                    // if we went past the end of the array wrap back around to 0;
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }
                }

            }
            else
            {
                ourButton.Input();
            }
           
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();


            // spriteBatch.Draw(ourCritter.image, Vector2.Zero, Color.White);
            foreach (Critter eachCritter in critterPool)
            {
                //loops contents 
                eachCritter.Draw(spriteBatch);

            }
            ourScore.Draw(spriteBatch);
            ourButton.Draw(spriteBatch);
            gameTimer.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        void Startgame()
        {
            playing = true;
            gameTimer.StartTimer();
            ourScore.resetScore();
        }

        void EndGame()
        {
            playing = false;
            // TODO:show button
            ourButton.Show();

            // despawn all critters 
            foreach(Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

            //reset timer
            gameTimer.SetTimer(GAME_LENGTH);
        }
    }
}
